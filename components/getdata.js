let express = require('express')
let mongodb = require('mongodb')
let client = mongodb.MongoClient
let ObjectId = mongodb.ObjectId

let get = express.Router().get('/:id',(req,res)=>{
    let objectId = ObjectId(req.params.id)
    client.connect('mongodb://localhost:27017/talentsprint',(err,db)=>{
        if(err){
            throw err
        }else{
           db.collection('userDetails').findOne({_id:objectId},(err,result)=>{
            if(err){
                throw err
            }else{
                res.send(result)
            }
           }) 
        }
    })
})

module.exports = get