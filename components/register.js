let express = require('express');
let mongodb = require('mongodb');
let client = mongodb.MongoClient;
let register = express.Router().post('/', async (req, res) => { 
    let data = req.body;
    try {
        const db = await client.connect('mongodb://localhost:27017/talentsprint');
        const existingUser = await db.collection('userDetails').findOne({ email: data.email });
        if (existingUser) {
            res.send({ message: "Email already Registered" });
        } else {
            const result = await db.collection('userDetails').insertOne(data);
            res.send({message:"Registration Successful"});
        }
        db.close();
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Internal server error' });
    }
});

module.exports = register;
