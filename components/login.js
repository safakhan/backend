let express = require('express')
let mongodb = require('mongodb')
let client = mongodb.MongoClient

let login = express.Router().post('/', async (req, res) => {
    let data = req.body
    try {
        const db = await client.connect('mongodb://localhost:27017/talentsprint')
        const user = await db.collection('userDetails').findOne(data);
        if (user) {
            if (user.email == 'admin@gmail.com' && user.password == 'admin1234') {
                res.send({ result: user, message: 'Admin logged in' });
            } else {
                res.send(user);
            }
        } else {
            res.send({ message: 'User not found' });
        }
    } catch (err) {
        console.error(err);
        res.status(500).send({ message: 'Internal server error' });
    }
})

module.exports = login
