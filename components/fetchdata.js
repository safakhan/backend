let express = require('express')
let mongodb = require('mongodb')
let client = mongodb.MongoClient

let fetch = express.Router().get('/',(req,res)=>{
    client.connect('mongodb://localhost:27017/talentsprint',(err,db)=>{
        if(err){
            throw err
        }else{
            db.collection('userDetails').find({}).toArray((err,result)=>{
                if(err){
                    throw err
                }else{
                    if (result.length > 0) {
                        res.send(result); //data will be sent to client (browser or Angular)
                    } else {
                        res.send({ message: "Record Not Found..." });
                    }
                }
            })
        }
    })
})

module.exports = fetch