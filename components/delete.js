let express = require('express')
let mongodb = require('mongodb')
let client = mongodb.MongoClient
const ObjectId = mongodb.ObjectId

let del = express.Router().delete('/:id',(req,res)=>{
    let objectId = ObjectId(req.params.id)
    client.connect('mongodb://localhost:27017/talentsprint',(err,db)=>{
        if(err){
            throw err
        }else{
            db.collection('userDetails').deleteOne({_id:objectId},(err,result)=>{
                if(err){
                    throw err
                }else{
                    res.send({result,message:"deleted"})
                }
            })
        }
    })
})

module.exports = del