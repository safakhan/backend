let express = require('express')
let bodyParser = require('body-parser')
let cors = require('cors')

let app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

app.use('/register',require('./components/register'))

app.use('/login',require('./components/login'))

app.use('/fetch',require('./components/fetchdata'))

app.use('/deletedata',require('./components/delete'))

app.use('/putdata',require('./components/update'))

app.use('/getdata', require('./components/getdata'))
app.use('/getbyemail' , require('./components/getbyemail'))

app.listen(8000,()=>{
    console.log("Running!!!")
})